package ru.romanorlov.data_collection_module.aspect;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import ru.romanorlov.data_collection_module.config.ApplicationConfig;

@Component
@Aspect
@Slf4j
@RequiredArgsConstructor
public class ApplicationControllerAspect {
    private final ApplicationConfig config;
    private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Before("execution(* ru.romanorlov.data_collection_module.controller.ApplicationController.getTheHistoryByTitle(..))")
    public void afterGetTheHistoryByTitleAdvice() {
        log.info("the controller has accepted a request for information: " + ZonedDateTime.now(ZoneId.of(
            config.getTimeZone())).format(FORMATTER));
    }
}
