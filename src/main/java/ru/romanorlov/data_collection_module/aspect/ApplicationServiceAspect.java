package ru.romanorlov.data_collection_module.aspect;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import ru.romanorlov.data_collection_module.config.ApplicationConfig;

@Component
@Aspect
@Slf4j
@RequiredArgsConstructor
public class ApplicationServiceAspect {
    private final ApplicationConfig config;
    private final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @AfterThrowing(pointcut = "execution(* ru.romanorlov.data_collection_module.service.application.ApplicationServiceImpl.getInfoAboutCurrencyPair(..))",
                   throwing = "exception")
    public void afterGetTheHistoryByTitleAdvice(Throwable exception) {
        log.warn("The user was trying to get information on a non-existent currency pair: "
                + exception.getMessage() + " "
                + ZonedDateTime.now(ZoneId.of(config.getTimeZone())).format(FORMATTER));
    }
}
