package ru.romanorlov.data_collection_module.client;

import java.util.Deque;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.romanorlov.data_collection_module.dto.client.ClientPriceValue;

@FeignClient(name = "generator-client", url = "${feign.client.config.generator-client.url}")
public interface GeneratorClient {
    @GetMapping(value = "/pair/{pairTitle}")
    Deque<ClientPriceValue> getPairByTitle(@PathVariable String pairTitle);

    @GetMapping(value = "/pair")
    List<String> getAvailablePairs();
}
