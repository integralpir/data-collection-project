package ru.romanorlov.data_collection_module.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.romanorlov.data_collection_module.dto.application.ApplicationCurrencyPair;
import ru.romanorlov.data_collection_module.service.application.ApplicationServiceImpl;

import java.util.Map;

@RestController
@RequestMapping(value = "/pair")
@RequiredArgsConstructor
@Tag(
    name = "Контроллер",
    description = "Доступные эндпоинты:"
)
public class ApplicationController {
    private final ApplicationServiceImpl service;

    @GetMapping(value = "/{pairTitle}")
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = ""
        + "Получение истории цен по конкретной валютной паре "
        + "за все время или "
        + "в определенных временных промежутках.")
    public ApplicationCurrencyPair getTheHistoryByTitle(
        @Parameter(
            description = "Временные промежутки",
            example = "{\"fromDate\":\"2023-02-17 12:15:00\", \"toDate\":\"2023-02-17 12:19:00\"}")
        @RequestParam Map<String, String> parameters,
        @Parameter(description = "Название валютной пары", example = "EURUSD")
        @PathVariable String pairTitle)
    {
        String fromDate = parameters.getOrDefault("fromDate", null);
        String toDate = parameters.getOrDefault("toDate", null);


        if (fromDate != null && toDate != null) {
            return service.getInfoAboutCurrencyPair(pairTitle, fromDate, toDate);
        } else if (fromDate != null) {
            return service.getInfoAboutCurrencyPair(pairTitle, fromDate);
        } else {
            return service.getInfoAboutCurrencyPair(pairTitle);
        }
    }
}
