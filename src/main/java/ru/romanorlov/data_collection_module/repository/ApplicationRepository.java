package ru.romanorlov.data_collection_module.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.romanorlov.data_collection_module.repository.entity.Title;

public interface ApplicationRepository extends JpaRepository<Title, String> {
}
