package ru.romanorlov.data_collection_module.repository.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Builder
@Entity
@Table(name = "trade")
public class Trade {
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pair_title")
    private Title pairTitle;

    @Column(name = "price")
    private double price;

    @Id
    @Column(name = "date_time")
    private LocalDateTime dateTime;

    public Trade(Title pairTitle, double price, LocalDateTime dateTime) {
        this.pairTitle = pairTitle;
        this.price = price;
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "price=" + price +
                ", dateTime=" + dateTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Trade trade = (Trade) o;
        return dateTime.equals(trade.getDateTime());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
