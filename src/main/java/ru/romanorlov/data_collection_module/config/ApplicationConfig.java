package ru.romanorlov.data_collection_module.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "config")
@Data
public class ApplicationConfig {
  private String timeZone;
}
