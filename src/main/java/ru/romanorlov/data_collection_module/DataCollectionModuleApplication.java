package ru.romanorlov.data_collection_module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.romanorlov.data_collection_module.config.ApplicationConfig;

@SpringBootApplication
@EnableFeignClients
@EnableScheduling
@EnableConfigurationProperties({ApplicationConfig.class})
public class DataCollectionModuleApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataCollectionModuleApplication.class, args);
    }
}
