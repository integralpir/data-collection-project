package ru.romanorlov.data_collection_module.service.application;

import ru.romanorlov.data_collection_module.dto.application.ApplicationCurrencyPair;

public interface ApplicationService {
    ApplicationCurrencyPair getInfoAboutCurrencyPair(String title);
    ApplicationCurrencyPair getInfoAboutCurrencyPair(String title, String fromDateString);
    ApplicationCurrencyPair getInfoAboutCurrencyPair(String title, String fromDateString, String toDateString);
}
