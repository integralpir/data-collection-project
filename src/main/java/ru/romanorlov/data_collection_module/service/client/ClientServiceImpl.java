package ru.romanorlov.data_collection_module.service.client;

import java.util.Deque;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.romanorlov.data_collection_module.client.GeneratorClient;
import ru.romanorlov.data_collection_module.dto.client.ClientPriceValue;
import ru.romanorlov.data_collection_module.repository.entity.Title;
import ru.romanorlov.data_collection_module.repository.entity.Trade;
import ru.romanorlov.data_collection_module.repository.ApplicationRepository;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl {
    private final GeneratorClient client;
    private final ApplicationRepository applicationRepository;

    @Scheduled(fixedDelay = 10000)
    private void updateInfoAboutTrades() {
        applicationRepository.findAll()
                .forEach(title -> {
                    Deque<ClientPriceValue> priceValues = client.getPairByTitle(title.getTitle());

                    priceValues.stream()
                        .map(priceValue -> convertInfoFromDtoToEntity(title, priceValue))
                        .forEach(title::addTrade);

                    applicationRepository.save(title);
                });
    }

    private Trade convertInfoFromDtoToEntity(Title title, ClientPriceValue price){
        return Trade.builder()
                .pairTitle(title)
                .price(price.getPrice())
                .dateTime(price.getDate())
                .build();
    }
}
