package ru.romanorlov.data_collection_module.service.client;

import org.springframework.scheduling.annotation.Scheduled;

public interface ClientService {
    @Scheduled(fixedDelay = 10000)
    void getInfoAboutTrades();
}
