package ru.romanorlov.data_collection_module.exception_handling;

public class CannotFindPairWithTitleException extends RuntimeException{
    public CannotFindPairWithTitleException(String message) {
        super(message);
    }
}
