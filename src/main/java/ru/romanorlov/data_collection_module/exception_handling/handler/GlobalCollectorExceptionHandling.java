package ru.romanorlov.data_collection_module.exception_handling.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.romanorlov.data_collection_module.exception_handling.CannotFindPairWithTitleException;

@RestControllerAdvice
public class GlobalCollectorExceptionHandling {

    @ExceptionHandler
    public ResponseEntity<String> handleException(CannotFindPairWithTitleException exception) {
        return new ResponseEntity<>(exception.getMessage(), HttpStatus.NOT_FOUND);
    }
}
