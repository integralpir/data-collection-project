package ru.romanorlov.data_collection_module.dto.client;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ClientPriceValue {
    private double price;
    private LocalDateTime date;
}
