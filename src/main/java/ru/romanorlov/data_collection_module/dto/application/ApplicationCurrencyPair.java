package ru.romanorlov.data_collection_module.dto.application;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class ApplicationCurrencyPair {
    private String title;
    private ApplicationPriceValue[] values;
}
