package ru.romanorlov.data_collection_module.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.romanorlov.data_collection_module.dto.application.ApplicationCurrencyPair;
import ru.romanorlov.data_collection_module.service.application.ApplicationServiceImpl;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ApplicationControllerTest {
    private static final String PAIR_TITLE = "TEST";
    private static final Map<String, String> PARAMETERS = new HashMap<>();
    @Mock
    private ApplicationServiceImpl service;

    @InjectMocks
    private ApplicationController controller;

    @Test
    void getTheHistoryByTitle_shouldCallService_withoutParameters() {
        final ApplicationCurrencyPair pair = mock(ApplicationCurrencyPair.class);
        when(service.getInfoAboutCurrencyPair(PAIR_TITLE)).thenReturn(pair);

        final ApplicationCurrencyPair actual = controller.getTheHistoryByTitle(PARAMETERS, PAIR_TITLE);

        assertNotNull(actual);
        assertEquals(pair, actual);
        verify(service).getInfoAboutCurrencyPair(PAIR_TITLE);
    }

    @Test
    void getTheHistoryByTitle_shouldCallService_withFromDate() {
        PARAMETERS.put("fromDate", "TEST");
        final ApplicationCurrencyPair pair = mock(ApplicationCurrencyPair.class);
        when(service.getInfoAboutCurrencyPair(PAIR_TITLE, PARAMETERS.get("fromDate"))).thenReturn(pair);

        final ApplicationCurrencyPair actual = controller.getTheHistoryByTitle(PARAMETERS, PAIR_TITLE);

        assertNotNull(actual);
        assertEquals(pair, actual);
        verify(service).getInfoAboutCurrencyPair(PAIR_TITLE, PARAMETERS.get("fromDate"));
        PARAMETERS.clear();
    }

    @Test
    void getTheHistoryByTitle_shouldCallService_withFromDateAndToDate() {
        PARAMETERS.put("fromDate", "fromDate");
        PARAMETERS.put("toDate", "toDate");
        final ApplicationCurrencyPair pair = mock(ApplicationCurrencyPair.class);
        when(service.getInfoAboutCurrencyPair(PAIR_TITLE, PARAMETERS.get("fromDate"), PARAMETERS.get("toDate"))).thenReturn(pair);

        final ApplicationCurrencyPair actual = controller.getTheHistoryByTitle(PARAMETERS, PAIR_TITLE);

        assertNotNull(actual);
        assertEquals(pair, actual);
        verify(service).getInfoAboutCurrencyPair(PAIR_TITLE, PARAMETERS.get("fromDate"), PARAMETERS.get("toDate"));
        PARAMETERS.clear();
    }
}
