package ru.romanorlov.data_collection_module.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.romanorlov.data_collection_module.client.GeneratorClient;
import ru.romanorlov.data_collection_module.repository.ApplicationRepository;
import ru.romanorlov.data_collection_module.service.client.ClientServiceImpl;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {
    @Mock
    private GeneratorClient client;
    @Mock
    private ApplicationRepository applicationRepository;

    @InjectMocks
    private ClientServiceImpl service;


}
