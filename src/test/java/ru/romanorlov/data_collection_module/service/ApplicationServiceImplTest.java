package ru.romanorlov.data_collection_module.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.romanorlov.data_collection_module.exception_handling.CannotFindPairWithTitleException;
import ru.romanorlov.data_collection_module.repository.ApplicationRepository;
import ru.romanorlov.data_collection_module.service.application.ApplicationServiceImpl;


import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith(MockitoExtension.class)
public class ApplicationServiceImplTest {
    @Mock
    private ApplicationRepository repository;
    @InjectMocks
    private ApplicationServiceImpl service;

    @Test
    void getInfoAboutCurrencyPair_shouldCallRepository() {
        assertThrows(CannotFindPairWithTitleException.class, () -> service.getInfoAboutCurrencyPair("wrong_title"));
    }
}
